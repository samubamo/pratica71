
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica71 {
    public static void main(String[] args) {
        Time time1 = new Time();
        Scanner scanner = new Scanner(System.in);
        int num_jogadores, cont = 0;
        boolean flag = true;
        
        System.out.println("Número de Jgadores: ");
        num_jogadores = scanner.nextInt();
        
        System.out.println("Numero Nome");
        for(int i = 0; i < num_jogadores; i++){
            time1.addJogador(new Jogador(scanner.nextInt(), scanner.next()));
        }
        
        JogadorComparator jc = new JogadorComparator(true,true, false);
        
       /* time1.addJogador(new Jogador(1, "Fernanda Garay"));
        time1.addJogador(new Jogador(2, "Tandara"));
        time1.addJogador(new Jogador(7, "Sheila"));*/
        
        time1.ordena(jc);
        
        for(Jogador time: time1.getJogadores()){
            System.out.println(time);
        }
        
        System.out.println("Numero Nome");
        for(cont = time1.getJogadores().size(); flag; cont++){
            time1.addJogador(new Jogador(scanner.nextInt(), scanner.next()));
            if(time1.getJogadores().get(cont).getNumero() == 0)
                flag = false;
           
        }
        time1.ordena(jc);
        for(Jogador time: time1.getJogadores()){
            System.out.println(time);
        }
        
        
    }
}
